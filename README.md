# ESTE PROYECTO ESTA FORMADO POR UN SOLO FORMULARIO
* MOVIMIENTOS DE LAS PIEZAS:
1- cada pieza se tiene que seleccionar previamente para poder moverla, una vez seleccionada se hara click en una de las casillas correspondientes en las que se pueda mover en ese momento
*TURNO 
1- Para controlar el turno, disponemos de un boton, igual que en la vida real cuando se juega al ajedrez, el cual cada vez que se seleccione, harà cambiar de color el turno de blancas o negras, ya que cada vez que u jugador haga un movimiento, acto seguido hay que darle al boton de turno.

*ESTADO DE LA PARTIDA
1- este apartado es muy simple, solo marca el estado en que se encuentra la partida, si activa o finalizada, ya que una vez que se haga finalizar partida, se cambiara de color la parte de partida finalizada.


*INCONVENIENTES DEL PROGRAMA
Entre los muchos inconvenientes que tiene este programa, resalta que no se puedan eliminar las piezas, que el turno no sea automatico, un contador con el tiempo y otro con las piezas eliminadas....



