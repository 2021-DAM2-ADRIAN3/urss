﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TORN = New System.Windows.Forms.Button()
        Me.TORNNEGRES = New System.Windows.Forms.Button()
        Me.TORNBLANQUES = New System.Windows.Forms.Button()
        Me.TORRENEGRA1 = New System.Windows.Forms.PictureBox()
        Me.A1 = New System.Windows.Forms.PictureBox()
        Me.E5 = New System.Windows.Forms.PictureBox()
        Me.E4 = New System.Windows.Forms.PictureBox()
        Me.E3 = New System.Windows.Forms.PictureBox()
        Me.E2 = New System.Windows.Forms.PictureBox()
        Me.E1 = New System.Windows.Forms.PictureBox()
        Me.D8 = New System.Windows.Forms.PictureBox()
        Me.D7 = New System.Windows.Forms.PictureBox()
        Me.D6 = New System.Windows.Forms.PictureBox()
        Me.D5 = New System.Windows.Forms.PictureBox()
        Me.D4 = New System.Windows.Forms.PictureBox()
        Me.D3 = New System.Windows.Forms.PictureBox()
        Me.D2 = New System.Windows.Forms.PictureBox()
        Me.D1 = New System.Windows.Forms.PictureBox()
        Me.C8 = New System.Windows.Forms.PictureBox()
        Me.C7 = New System.Windows.Forms.PictureBox()
        Me.C6 = New System.Windows.Forms.PictureBox()
        Me.C5 = New System.Windows.Forms.PictureBox()
        Me.C4 = New System.Windows.Forms.PictureBox()
        Me.C3 = New System.Windows.Forms.PictureBox()
        Me.C2 = New System.Windows.Forms.PictureBox()
        Me.C1 = New System.Windows.Forms.PictureBox()
        Me.B8 = New System.Windows.Forms.PictureBox()
        Me.B7 = New System.Windows.Forms.PictureBox()
        Me.B6 = New System.Windows.Forms.PictureBox()
        Me.B5 = New System.Windows.Forms.PictureBox()
        Me.B4 = New System.Windows.Forms.PictureBox()
        Me.B3 = New System.Windows.Forms.PictureBox()
        Me.B2 = New System.Windows.Forms.PictureBox()
        Me.B1 = New System.Windows.Forms.PictureBox()
        Me.A8 = New System.Windows.Forms.PictureBox()
        Me.A7 = New System.Windows.Forms.PictureBox()
        Me.A6 = New System.Windows.Forms.PictureBox()
        Me.A5 = New System.Windows.Forms.PictureBox()
        Me.A4 = New System.Windows.Forms.PictureBox()
        Me.A3 = New System.Windows.Forms.PictureBox()
        Me.A2 = New System.Windows.Forms.PictureBox()
        Me.F4 = New System.Windows.Forms.PictureBox()
        Me.F3 = New System.Windows.Forms.PictureBox()
        Me.F2 = New System.Windows.Forms.PictureBox()
        Me.F1 = New System.Windows.Forms.PictureBox()
        Me.E8 = New System.Windows.Forms.PictureBox()
        Me.E7 = New System.Windows.Forms.PictureBox()
        Me.E6 = New System.Windows.Forms.PictureBox()
        Me.G2 = New System.Windows.Forms.PictureBox()
        Me.G1 = New System.Windows.Forms.PictureBox()
        Me.F8 = New System.Windows.Forms.PictureBox()
        Me.F7 = New System.Windows.Forms.PictureBox()
        Me.F6 = New System.Windows.Forms.PictureBox()
        Me.F5 = New System.Windows.Forms.PictureBox()
        Me.G3 = New System.Windows.Forms.PictureBox()
        Me.G4 = New System.Windows.Forms.PictureBox()
        Me.G7 = New System.Windows.Forms.PictureBox()
        Me.G6 = New System.Windows.Forms.PictureBox()
        Me.G5 = New System.Windows.Forms.PictureBox()
        Me.G8 = New System.Windows.Forms.PictureBox()
        Me.H1 = New System.Windows.Forms.PictureBox()
        Me.H3 = New System.Windows.Forms.PictureBox()
        Me.H2 = New System.Windows.Forms.PictureBox()
        Me.H4 = New System.Windows.Forms.PictureBox()
        Me.H6 = New System.Windows.Forms.PictureBox()
        Me.H7 = New System.Windows.Forms.PictureBox()
        Me.H8 = New System.Windows.Forms.PictureBox()
        Me.H5 = New System.Windows.Forms.PictureBox()
        Me.TORRENEGRA2 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC1 = New System.Windows.Forms.PictureBox()
        Me.REIBLANC = New System.Windows.Forms.PictureBox()
        Me.CAVALLBLANC1 = New System.Windows.Forms.PictureBox()
        Me.ALFILBLANC1 = New System.Windows.Forms.PictureBox()
        Me.REINABLANCA = New System.Windows.Forms.PictureBox()
        Me.ALFILNEGRE1 = New System.Windows.Forms.PictureBox()
        Me.REINANEGRA = New System.Windows.Forms.PictureBox()
        Me.TORREBLANCA1 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE1 = New System.Windows.Forms.PictureBox()
        Me.CAVALLNEGRE1 = New System.Windows.Forms.PictureBox()
        Me.REINEGRE = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE8 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE7 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE6 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE5 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE4 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE3 = New System.Windows.Forms.PictureBox()
        Me.CAVALLNEGRE2 = New System.Windows.Forms.PictureBox()
        Me.PEONEGRE2 = New System.Windows.Forms.PictureBox()
        Me.ALFILNEGRE2 = New System.Windows.Forms.PictureBox()
        Me.TORREBLANCA2 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC8 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC7 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC6 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC5 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC4 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC3 = New System.Windows.Forms.PictureBox()
        Me.PEOBLANC2 = New System.Windows.Forms.PictureBox()
        Me.ALFILBLANC2 = New System.Windows.Forms.PictureBox()
        Me.CAVALLBLANC2 = New System.Windows.Forms.PictureBox()
        Me.TAULELL = New System.Windows.Forms.PictureBox()
        Me.FINALITZAR = New System.Windows.Forms.Button()
        Me.PARTIDAACTIVA = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.PARTIDAFINALITZADA = New System.Windows.Forms.Button()
        CType(Me.TORRENEGRA1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TORRENEGRA2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.REIBLANC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CAVALLBLANC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALFILBLANC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.REINABLANCA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALFILNEGRE1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.REINANEGRA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TORREBLANCA1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CAVALLNEGRE1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.REINEGRE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CAVALLNEGRE2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEONEGRE2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALFILNEGRE2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TORREBLANCA2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PEOBLANC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALFILBLANC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CAVALLBLANC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TAULELL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TORN
        '
        Me.TORN.Location = New System.Drawing.Point(514, 12)
        Me.TORN.Name = "TORN"
        Me.TORN.Size = New System.Drawing.Size(75, 23)
        Me.TORN.TabIndex = 1
        Me.TORN.Text = "TORN"
        Me.TORN.UseVisualStyleBackColor = True
        '
        'TORNNEGRES
        '
        Me.TORNNEGRES.Location = New System.Drawing.Point(455, 41)
        Me.TORNNEGRES.Name = "TORNNEGRES"
        Me.TORNNEGRES.Size = New System.Drawing.Size(75, 23)
        Me.TORNNEGRES.TabIndex = 2
        Me.TORNNEGRES.Text = "NEGRES"
        Me.TORNNEGRES.UseVisualStyleBackColor = True
        '
        'TORNBLANQUES
        '
        Me.TORNBLANQUES.Location = New System.Drawing.Point(571, 41)
        Me.TORNBLANQUES.Name = "TORNBLANQUES"
        Me.TORNBLANQUES.Size = New System.Drawing.Size(75, 23)
        Me.TORNBLANQUES.TabIndex = 3
        Me.TORNBLANQUES.Text = "BLANQUES"
        Me.TORNBLANQUES.UseVisualStyleBackColor = True
        '
        'TORRENEGRA1
        '
        Me.TORRENEGRA1.Image = CType(resources.GetObject("TORRENEGRA1.Image"), System.Drawing.Image)
        Me.TORRENEGRA1.Location = New System.Drawing.Point(375, 12)
        Me.TORRENEGRA1.Name = "TORRENEGRA1"
        Me.TORRENEGRA1.Size = New System.Drawing.Size(44, 41)
        Me.TORRENEGRA1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.TORRENEGRA1.TabIndex = 4
        Me.TORRENEGRA1.TabStop = False
        Me.TORRENEGRA1.Tag = "19"
        '
        'A1
        '
        Me.A1.Location = New System.Drawing.Point(16, 351)
        Me.A1.Name = "A1"
        Me.A1.Size = New System.Drawing.Size(42, 41)
        Me.A1.TabIndex = 5
        Me.A1.TabStop = False
        '
        'E5
        '
        Me.E5.Location = New System.Drawing.Point(217, 160)
        Me.E5.Name = "E5"
        Me.E5.Size = New System.Drawing.Size(42, 41)
        Me.E5.TabIndex = 6
        Me.E5.TabStop = False
        '
        'E4
        '
        Me.E4.Location = New System.Drawing.Point(217, 206)
        Me.E4.Name = "E4"
        Me.E4.Size = New System.Drawing.Size(42, 41)
        Me.E4.TabIndex = 7
        Me.E4.TabStop = False
        '
        'E3
        '
        Me.E3.Location = New System.Drawing.Point(217, 255)
        Me.E3.Name = "E3"
        Me.E3.Size = New System.Drawing.Size(42, 41)
        Me.E3.TabIndex = 8
        Me.E3.TabStop = False
        '
        'E2
        '
        Me.E2.Location = New System.Drawing.Point(217, 302)
        Me.E2.Name = "E2"
        Me.E2.Size = New System.Drawing.Size(42, 41)
        Me.E2.TabIndex = 9
        Me.E2.TabStop = False
        '
        'E1
        '
        Me.E1.Location = New System.Drawing.Point(217, 351)
        Me.E1.Name = "E1"
        Me.E1.Size = New System.Drawing.Size(42, 41)
        Me.E1.TabIndex = 10
        Me.E1.TabStop = False
        '
        'D8
        '
        Me.D8.Location = New System.Drawing.Point(169, 12)
        Me.D8.Name = "D8"
        Me.D8.Size = New System.Drawing.Size(42, 41)
        Me.D8.TabIndex = 11
        Me.D8.TabStop = False
        '
        'D7
        '
        Me.D7.Location = New System.Drawing.Point(169, 64)
        Me.D7.Name = "D7"
        Me.D7.Size = New System.Drawing.Size(42, 41)
        Me.D7.TabIndex = 12
        Me.D7.TabStop = False
        '
        'D6
        '
        Me.D6.Location = New System.Drawing.Point(169, 112)
        Me.D6.Name = "D6"
        Me.D6.Size = New System.Drawing.Size(42, 41)
        Me.D6.TabIndex = 13
        Me.D6.TabStop = False
        '
        'D5
        '
        Me.D5.Location = New System.Drawing.Point(169, 159)
        Me.D5.Name = "D5"
        Me.D5.Size = New System.Drawing.Size(42, 41)
        Me.D5.TabIndex = 14
        Me.D5.TabStop = False
        '
        'D4
        '
        Me.D4.Location = New System.Drawing.Point(169, 206)
        Me.D4.Name = "D4"
        Me.D4.Size = New System.Drawing.Size(42, 41)
        Me.D4.TabIndex = 15
        Me.D4.TabStop = False
        '
        'D3
        '
        Me.D3.Location = New System.Drawing.Point(169, 257)
        Me.D3.Name = "D3"
        Me.D3.Size = New System.Drawing.Size(42, 41)
        Me.D3.TabIndex = 16
        Me.D3.TabStop = False
        '
        'D2
        '
        Me.D2.Location = New System.Drawing.Point(169, 304)
        Me.D2.Name = "D2"
        Me.D2.Size = New System.Drawing.Size(42, 41)
        Me.D2.TabIndex = 17
        Me.D2.TabStop = False
        '
        'D1
        '
        Me.D1.Location = New System.Drawing.Point(169, 351)
        Me.D1.Name = "D1"
        Me.D1.Size = New System.Drawing.Size(42, 41)
        Me.D1.TabIndex = 18
        Me.D1.TabStop = False
        '
        'C8
        '
        Me.C8.Location = New System.Drawing.Point(121, 12)
        Me.C8.Name = "C8"
        Me.C8.Size = New System.Drawing.Size(42, 41)
        Me.C8.TabIndex = 19
        Me.C8.TabStop = False
        '
        'C7
        '
        Me.C7.Location = New System.Drawing.Point(121, 59)
        Me.C7.Name = "C7"
        Me.C7.Size = New System.Drawing.Size(42, 41)
        Me.C7.TabIndex = 20
        Me.C7.TabStop = False
        '
        'C6
        '
        Me.C6.Location = New System.Drawing.Point(119, 110)
        Me.C6.Name = "C6"
        Me.C6.Size = New System.Drawing.Size(42, 41)
        Me.C6.TabIndex = 21
        Me.C6.TabStop = False
        '
        'C5
        '
        Me.C5.Location = New System.Drawing.Point(121, 160)
        Me.C5.Name = "C5"
        Me.C5.Size = New System.Drawing.Size(42, 41)
        Me.C5.TabIndex = 22
        Me.C5.TabStop = False
        '
        'C4
        '
        Me.C4.Location = New System.Drawing.Point(121, 207)
        Me.C4.Name = "C4"
        Me.C4.Size = New System.Drawing.Size(42, 41)
        Me.C4.TabIndex = 23
        Me.C4.TabStop = False
        '
        'C3
        '
        Me.C3.Location = New System.Drawing.Point(121, 254)
        Me.C3.Name = "C3"
        Me.C3.Size = New System.Drawing.Size(42, 41)
        Me.C3.TabIndex = 24
        Me.C3.TabStop = False
        '
        'C2
        '
        Me.C2.Location = New System.Drawing.Point(119, 301)
        Me.C2.Name = "C2"
        Me.C2.Size = New System.Drawing.Size(42, 41)
        Me.C2.TabIndex = 25
        Me.C2.TabStop = False
        '
        'C1
        '
        Me.C1.Location = New System.Drawing.Point(121, 351)
        Me.C1.Name = "C1"
        Me.C1.Size = New System.Drawing.Size(42, 41)
        Me.C1.TabIndex = 26
        Me.C1.TabStop = False
        '
        'B8
        '
        Me.B8.Location = New System.Drawing.Point(64, 12)
        Me.B8.Name = "B8"
        Me.B8.Size = New System.Drawing.Size(42, 41)
        Me.B8.TabIndex = 27
        Me.B8.TabStop = False
        '
        'B7
        '
        Me.B7.Location = New System.Drawing.Point(64, 59)
        Me.B7.Name = "B7"
        Me.B7.Size = New System.Drawing.Size(42, 41)
        Me.B7.TabIndex = 28
        Me.B7.TabStop = False
        '
        'B6
        '
        Me.B6.Location = New System.Drawing.Point(64, 113)
        Me.B6.Name = "B6"
        Me.B6.Size = New System.Drawing.Size(42, 41)
        Me.B6.TabIndex = 29
        Me.B6.TabStop = False
        '
        'B5
        '
        Me.B5.Location = New System.Drawing.Point(64, 160)
        Me.B5.Name = "B5"
        Me.B5.Size = New System.Drawing.Size(42, 41)
        Me.B5.TabIndex = 30
        Me.B5.TabStop = False
        '
        'B4
        '
        Me.B4.Location = New System.Drawing.Point(64, 206)
        Me.B4.Name = "B4"
        Me.B4.Size = New System.Drawing.Size(42, 41)
        Me.B4.TabIndex = 31
        Me.B4.TabStop = False
        '
        'B3
        '
        Me.B3.Location = New System.Drawing.Point(64, 255)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(42, 41)
        Me.B3.TabIndex = 32
        Me.B3.TabStop = False
        '
        'B2
        '
        Me.B2.Location = New System.Drawing.Point(64, 302)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(42, 41)
        Me.B2.TabIndex = 33
        Me.B2.TabStop = False
        '
        'B1
        '
        Me.B1.Location = New System.Drawing.Point(64, 351)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(42, 41)
        Me.B1.TabIndex = 34
        Me.B1.TabStop = False
        '
        'A8
        '
        Me.A8.Location = New System.Drawing.Point(16, 12)
        Me.A8.Name = "A8"
        Me.A8.Size = New System.Drawing.Size(42, 41)
        Me.A8.TabIndex = 35
        Me.A8.TabStop = False
        '
        'A7
        '
        Me.A7.Location = New System.Drawing.Point(16, 59)
        Me.A7.Name = "A7"
        Me.A7.Size = New System.Drawing.Size(42, 41)
        Me.A7.TabIndex = 36
        Me.A7.TabStop = False
        '
        'A6
        '
        Me.A6.Location = New System.Drawing.Point(16, 113)
        Me.A6.Name = "A6"
        Me.A6.Size = New System.Drawing.Size(42, 41)
        Me.A6.TabIndex = 37
        Me.A6.TabStop = False
        '
        'A5
        '
        Me.A5.Location = New System.Drawing.Point(16, 160)
        Me.A5.Name = "A5"
        Me.A5.Size = New System.Drawing.Size(42, 41)
        Me.A5.TabIndex = 38
        Me.A5.TabStop = False
        '
        'A4
        '
        Me.A4.Location = New System.Drawing.Point(16, 207)
        Me.A4.Name = "A4"
        Me.A4.Size = New System.Drawing.Size(42, 41)
        Me.A4.TabIndex = 39
        Me.A4.TabStop = False
        '
        'A3
        '
        Me.A3.Location = New System.Drawing.Point(16, 254)
        Me.A3.Name = "A3"
        Me.A3.Size = New System.Drawing.Size(42, 41)
        Me.A3.TabIndex = 40
        Me.A3.TabStop = False
        '
        'A2
        '
        Me.A2.Location = New System.Drawing.Point(16, 304)
        Me.A2.Name = "A2"
        Me.A2.Size = New System.Drawing.Size(42, 41)
        Me.A2.TabIndex = 41
        Me.A2.TabStop = False
        '
        'F4
        '
        Me.F4.Location = New System.Drawing.Point(265, 207)
        Me.F4.Name = "F4"
        Me.F4.Size = New System.Drawing.Size(42, 41)
        Me.F4.TabIndex = 42
        Me.F4.TabStop = False
        '
        'F3
        '
        Me.F3.Location = New System.Drawing.Point(265, 254)
        Me.F3.Name = "F3"
        Me.F3.Size = New System.Drawing.Size(42, 41)
        Me.F3.TabIndex = 43
        Me.F3.TabStop = False
        '
        'F2
        '
        Me.F2.Location = New System.Drawing.Point(265, 302)
        Me.F2.Name = "F2"
        Me.F2.Size = New System.Drawing.Size(42, 41)
        Me.F2.TabIndex = 44
        Me.F2.TabStop = False
        '
        'F1
        '
        Me.F1.Location = New System.Drawing.Point(265, 351)
        Me.F1.Name = "F1"
        Me.F1.Size = New System.Drawing.Size(42, 41)
        Me.F1.TabIndex = 45
        Me.F1.TabStop = False
        '
        'E8
        '
        Me.E8.Location = New System.Drawing.Point(217, 12)
        Me.E8.Name = "E8"
        Me.E8.Size = New System.Drawing.Size(42, 41)
        Me.E8.TabIndex = 46
        Me.E8.TabStop = False
        '
        'E7
        '
        Me.E7.Location = New System.Drawing.Point(217, 66)
        Me.E7.Name = "E7"
        Me.E7.Size = New System.Drawing.Size(42, 41)
        Me.E7.TabIndex = 47
        Me.E7.TabStop = False
        '
        'E6
        '
        Me.E6.Location = New System.Drawing.Point(217, 113)
        Me.E6.Name = "E6"
        Me.E6.Size = New System.Drawing.Size(42, 41)
        Me.E6.TabIndex = 48
        Me.E6.TabStop = False
        '
        'G2
        '
        Me.G2.Location = New System.Drawing.Point(325, 302)
        Me.G2.Name = "G2"
        Me.G2.Size = New System.Drawing.Size(42, 41)
        Me.G2.TabIndex = 49
        Me.G2.TabStop = False
        '
        'G1
        '
        Me.G1.Location = New System.Drawing.Point(325, 351)
        Me.G1.Name = "G1"
        Me.G1.Size = New System.Drawing.Size(42, 41)
        Me.G1.TabIndex = 50
        Me.G1.TabStop = False
        '
        'F8
        '
        Me.F8.Location = New System.Drawing.Point(273, 12)
        Me.F8.Name = "F8"
        Me.F8.Size = New System.Drawing.Size(42, 41)
        Me.F8.TabIndex = 51
        Me.F8.TabStop = False
        '
        'F7
        '
        Me.F7.Location = New System.Drawing.Point(267, 64)
        Me.F7.Name = "F7"
        Me.F7.Size = New System.Drawing.Size(42, 41)
        Me.F7.TabIndex = 52
        Me.F7.TabStop = False
        '
        'F6
        '
        Me.F6.Location = New System.Drawing.Point(275, 111)
        Me.F6.Name = "F6"
        Me.F6.Size = New System.Drawing.Size(42, 41)
        Me.F6.TabIndex = 53
        Me.F6.TabStop = False
        '
        'F5
        '
        Me.F5.Location = New System.Drawing.Point(275, 158)
        Me.F5.Name = "F5"
        Me.F5.Size = New System.Drawing.Size(42, 41)
        Me.F5.TabIndex = 54
        Me.F5.TabStop = False
        '
        'G3
        '
        Me.G3.Location = New System.Drawing.Point(325, 257)
        Me.G3.Name = "G3"
        Me.G3.Size = New System.Drawing.Size(42, 41)
        Me.G3.TabIndex = 55
        Me.G3.TabStop = False
        '
        'G4
        '
        Me.G4.Location = New System.Drawing.Point(324, 206)
        Me.G4.Name = "G4"
        Me.G4.Size = New System.Drawing.Size(42, 41)
        Me.G4.TabIndex = 56
        Me.G4.TabStop = False
        '
        'G7
        '
        Me.G7.Location = New System.Drawing.Point(323, 65)
        Me.G7.Name = "G7"
        Me.G7.Size = New System.Drawing.Size(42, 41)
        Me.G7.TabIndex = 57
        Me.G7.TabStop = False
        '
        'G6
        '
        Me.G6.Location = New System.Drawing.Point(323, 111)
        Me.G6.Name = "G6"
        Me.G6.Size = New System.Drawing.Size(42, 41)
        Me.G6.TabIndex = 58
        Me.G6.TabStop = False
        '
        'G5
        '
        Me.G5.Location = New System.Drawing.Point(323, 159)
        Me.G5.Name = "G5"
        Me.G5.Size = New System.Drawing.Size(42, 41)
        Me.G5.TabIndex = 59
        Me.G5.TabStop = False
        '
        'G8
        '
        Me.G8.Location = New System.Drawing.Point(327, 12)
        Me.G8.Name = "G8"
        Me.G8.Size = New System.Drawing.Size(42, 41)
        Me.G8.TabIndex = 60
        Me.G8.TabStop = False
        '
        'H1
        '
        Me.H1.Location = New System.Drawing.Point(377, 349)
        Me.H1.Name = "H1"
        Me.H1.Size = New System.Drawing.Size(42, 41)
        Me.H1.TabIndex = 61
        Me.H1.TabStop = False
        '
        'H3
        '
        Me.H3.Location = New System.Drawing.Point(373, 254)
        Me.H3.Name = "H3"
        Me.H3.Size = New System.Drawing.Size(42, 41)
        Me.H3.TabIndex = 62
        Me.H3.TabStop = False
        '
        'H2
        '
        Me.H2.Location = New System.Drawing.Point(373, 302)
        Me.H2.Name = "H2"
        Me.H2.Size = New System.Drawing.Size(42, 41)
        Me.H2.TabIndex = 63
        Me.H2.TabStop = False
        '
        'H4
        '
        Me.H4.Location = New System.Drawing.Point(372, 206)
        Me.H4.Name = "H4"
        Me.H4.Size = New System.Drawing.Size(42, 41)
        Me.H4.TabIndex = 64
        Me.H4.TabStop = False
        '
        'H6
        '
        Me.H6.Location = New System.Drawing.Point(373, 111)
        Me.H6.Name = "H6"
        Me.H6.Size = New System.Drawing.Size(42, 41)
        Me.H6.TabIndex = 65
        Me.H6.TabStop = False
        '
        'H7
        '
        Me.H7.Location = New System.Drawing.Point(371, 64)
        Me.H7.Name = "H7"
        Me.H7.Size = New System.Drawing.Size(42, 41)
        Me.H7.TabIndex = 66
        Me.H7.TabStop = False
        '
        'H8
        '
        Me.H8.Location = New System.Drawing.Point(373, 12)
        Me.H8.Name = "H8"
        Me.H8.Size = New System.Drawing.Size(42, 41)
        Me.H8.TabIndex = 67
        Me.H8.TabStop = False
        '
        'H5
        '
        Me.H5.Location = New System.Drawing.Point(371, 158)
        Me.H5.Name = "H5"
        Me.H5.Size = New System.Drawing.Size(42, 41)
        Me.H5.TabIndex = 68
        Me.H5.TabStop = False
        '
        'TORRENEGRA2
        '
        Me.TORRENEGRA2.Image = CType(resources.GetObject("TORRENEGRA2.Image"), System.Drawing.Image)
        Me.TORRENEGRA2.Location = New System.Drawing.Point(16, 12)
        Me.TORRENEGRA2.Name = "TORRENEGRA2"
        Me.TORRENEGRA2.Size = New System.Drawing.Size(44, 41)
        Me.TORRENEGRA2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.TORRENEGRA2.TabIndex = 69
        Me.TORRENEGRA2.TabStop = False
        Me.TORRENEGRA2.Tag = "20"
        '
        'PEOBLANC1
        '
        Me.PEOBLANC1.Image = CType(resources.GetObject("PEOBLANC1.Image"), System.Drawing.Image)
        Me.PEOBLANC1.Location = New System.Drawing.Point(375, 301)
        Me.PEOBLANC1.Name = "PEOBLANC1"
        Me.PEOBLANC1.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC1.TabIndex = 70
        Me.PEOBLANC1.TabStop = False
        Me.PEOBLANC1.Tag = "1"
        '
        'REIBLANC
        '
        Me.REIBLANC.Image = CType(resources.GetObject("REIBLANC.Image"), System.Drawing.Image)
        Me.REIBLANC.Location = New System.Drawing.Point(219, 349)
        Me.REIBLANC.Name = "REIBLANC"
        Me.REIBLANC.Size = New System.Drawing.Size(44, 41)
        Me.REIBLANC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.REIBLANC.TabIndex = 71
        Me.REIBLANC.TabStop = False
        Me.REIBLANC.Tag = "31"
        '
        'CAVALLBLANC1
        '
        Me.CAVALLBLANC1.Image = CType(resources.GetObject("CAVALLBLANC1.Image"), System.Drawing.Image)
        Me.CAVALLBLANC1.Location = New System.Drawing.Point(327, 351)
        Me.CAVALLBLANC1.Name = "CAVALLBLANC1"
        Me.CAVALLBLANC1.Size = New System.Drawing.Size(44, 41)
        Me.CAVALLBLANC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CAVALLBLANC1.TabIndex = 72
        Me.CAVALLBLANC1.TabStop = False
        Me.CAVALLBLANC1.Tag = "21"
        '
        'ALFILBLANC1
        '
        Me.ALFILBLANC1.Image = CType(resources.GetObject("ALFILBLANC1.Image"), System.Drawing.Image)
        Me.ALFILBLANC1.Location = New System.Drawing.Point(265, 348)
        Me.ALFILBLANC1.Name = "ALFILBLANC1"
        Me.ALFILBLANC1.Size = New System.Drawing.Size(44, 41)
        Me.ALFILBLANC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ALFILBLANC1.TabIndex = 77
        Me.ALFILBLANC1.TabStop = False
        Me.ALFILBLANC1.Tag = "25"
        '
        'REINABLANCA
        '
        Me.REINABLANCA.Image = CType(resources.GetObject("REINABLANCA.Image"), System.Drawing.Image)
        Me.REINABLANCA.Location = New System.Drawing.Point(169, 349)
        Me.REINABLANCA.Name = "REINABLANCA"
        Me.REINABLANCA.Size = New System.Drawing.Size(44, 41)
        Me.REINABLANCA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.REINABLANCA.TabIndex = 78
        Me.REINABLANCA.TabStop = False
        Me.REINABLANCA.Tag = "29"
        '
        'ALFILNEGRE1
        '
        Me.ALFILNEGRE1.Image = CType(resources.GetObject("ALFILNEGRE1.Image"), System.Drawing.Image)
        Me.ALFILNEGRE1.Location = New System.Drawing.Point(277, 12)
        Me.ALFILNEGRE1.Name = "ALFILNEGRE1"
        Me.ALFILNEGRE1.Size = New System.Drawing.Size(44, 41)
        Me.ALFILNEGRE1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ALFILNEGRE1.TabIndex = 81
        Me.ALFILNEGRE1.TabStop = False
        Me.ALFILNEGRE1.Tag = "27"
        '
        'REINANEGRA
        '
        Me.REINANEGRA.Image = CType(resources.GetObject("REINANEGRA.Image"), System.Drawing.Image)
        Me.REINANEGRA.Location = New System.Drawing.Point(165, 12)
        Me.REINANEGRA.Name = "REINANEGRA"
        Me.REINANEGRA.Size = New System.Drawing.Size(44, 41)
        Me.REINANEGRA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.REINANEGRA.TabIndex = 82
        Me.REINANEGRA.TabStop = False
        Me.REINANEGRA.Tag = "30"
        '
        'TORREBLANCA1
        '
        Me.TORREBLANCA1.Image = CType(resources.GetObject("TORREBLANCA1.Image"), System.Drawing.Image)
        Me.TORREBLANCA1.Location = New System.Drawing.Point(377, 351)
        Me.TORREBLANCA1.Name = "TORREBLANCA1"
        Me.TORREBLANCA1.Size = New System.Drawing.Size(44, 41)
        Me.TORREBLANCA1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.TORREBLANCA1.TabIndex = 83
        Me.TORREBLANCA1.TabStop = False
        Me.TORREBLANCA1.Tag = "17"
        '
        'PEONEGRE1
        '
        Me.PEONEGRE1.Image = CType(resources.GetObject("PEONEGRE1.Image"), System.Drawing.Image)
        Me.PEONEGRE1.Location = New System.Drawing.Point(371, 59)
        Me.PEONEGRE1.Name = "PEONEGRE1"
        Me.PEONEGRE1.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE1.TabIndex = 89
        Me.PEONEGRE1.TabStop = False
        Me.PEONEGRE1.Tag = "9"
        '
        'CAVALLNEGRE1
        '
        Me.CAVALLNEGRE1.Image = CType(resources.GetObject("CAVALLNEGRE1.Image"), System.Drawing.Image)
        Me.CAVALLNEGRE1.Location = New System.Drawing.Point(327, 12)
        Me.CAVALLNEGRE1.Name = "CAVALLNEGRE1"
        Me.CAVALLNEGRE1.Size = New System.Drawing.Size(44, 41)
        Me.CAVALLNEGRE1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CAVALLNEGRE1.TabIndex = 90
        Me.CAVALLNEGRE1.TabStop = False
        Me.CAVALLNEGRE1.Tag = "23"
        '
        'REINEGRE
        '
        Me.REINEGRE.Image = CType(resources.GetObject("REINEGRE.Image"), System.Drawing.Image)
        Me.REINEGRE.Location = New System.Drawing.Point(215, 12)
        Me.REINEGRE.Name = "REINEGRE"
        Me.REINEGRE.Size = New System.Drawing.Size(44, 41)
        Me.REINEGRE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.REINEGRE.TabIndex = 91
        Me.REINEGRE.TabStop = False
        Me.REINEGRE.Tag = "32"
        '
        'PEONEGRE8
        '
        Me.PEONEGRE8.Image = CType(resources.GetObject("PEONEGRE8.Image"), System.Drawing.Image)
        Me.PEONEGRE8.Location = New System.Drawing.Point(14, 59)
        Me.PEONEGRE8.Name = "PEONEGRE8"
        Me.PEONEGRE8.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE8.TabIndex = 94
        Me.PEONEGRE8.TabStop = False
        Me.PEONEGRE8.Tag = "16"
        '
        'PEONEGRE7
        '
        Me.PEONEGRE7.Image = CType(resources.GetObject("PEONEGRE7.Image"), System.Drawing.Image)
        Me.PEONEGRE7.Location = New System.Drawing.Point(64, 64)
        Me.PEONEGRE7.Name = "PEONEGRE7"
        Me.PEONEGRE7.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE7.TabIndex = 95
        Me.PEONEGRE7.TabStop = False
        Me.PEONEGRE7.Tag = "15"
        '
        'PEONEGRE6
        '
        Me.PEONEGRE6.Image = CType(resources.GetObject("PEONEGRE6.Image"), System.Drawing.Image)
        Me.PEONEGRE6.Location = New System.Drawing.Point(121, 59)
        Me.PEONEGRE6.Name = "PEONEGRE6"
        Me.PEONEGRE6.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE6.TabIndex = 96
        Me.PEONEGRE6.TabStop = False
        Me.PEONEGRE6.Tag = "14"
        '
        'PEONEGRE5
        '
        Me.PEONEGRE5.Image = CType(resources.GetObject("PEONEGRE5.Image"), System.Drawing.Image)
        Me.PEONEGRE5.Location = New System.Drawing.Point(169, 65)
        Me.PEONEGRE5.Name = "PEONEGRE5"
        Me.PEONEGRE5.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE5.TabIndex = 97
        Me.PEONEGRE5.TabStop = False
        Me.PEONEGRE5.Tag = "13"
        '
        'PEONEGRE4
        '
        Me.PEONEGRE4.Image = CType(resources.GetObject("PEONEGRE4.Image"), System.Drawing.Image)
        Me.PEONEGRE4.Location = New System.Drawing.Point(217, 64)
        Me.PEONEGRE4.Name = "PEONEGRE4"
        Me.PEONEGRE4.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE4.TabIndex = 98
        Me.PEONEGRE4.TabStop = False
        Me.PEONEGRE4.Tag = "12"
        '
        'PEONEGRE3
        '
        Me.PEONEGRE3.Image = CType(resources.GetObject("PEONEGRE3.Image"), System.Drawing.Image)
        Me.PEONEGRE3.Location = New System.Drawing.Point(267, 64)
        Me.PEONEGRE3.Name = "PEONEGRE3"
        Me.PEONEGRE3.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE3.TabIndex = 99
        Me.PEONEGRE3.TabStop = False
        Me.PEONEGRE3.Tag = "11"
        '
        'CAVALLNEGRE2
        '
        Me.CAVALLNEGRE2.Image = CType(resources.GetObject("CAVALLNEGRE2.Image"), System.Drawing.Image)
        Me.CAVALLNEGRE2.Location = New System.Drawing.Point(64, 12)
        Me.CAVALLNEGRE2.Name = "CAVALLNEGRE2"
        Me.CAVALLNEGRE2.Size = New System.Drawing.Size(44, 41)
        Me.CAVALLNEGRE2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CAVALLNEGRE2.TabIndex = 100
        Me.CAVALLNEGRE2.TabStop = False
        Me.CAVALLNEGRE2.Tag = "24"
        '
        'PEONEGRE2
        '
        Me.PEONEGRE2.Image = CType(resources.GetObject("PEONEGRE2.Image"), System.Drawing.Image)
        Me.PEONEGRE2.Location = New System.Drawing.Point(319, 65)
        Me.PEONEGRE2.Name = "PEONEGRE2"
        Me.PEONEGRE2.Size = New System.Drawing.Size(44, 41)
        Me.PEONEGRE2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEONEGRE2.TabIndex = 101
        Me.PEONEGRE2.TabStop = False
        Me.PEONEGRE2.Tag = "10"
        '
        'ALFILNEGRE2
        '
        Me.ALFILNEGRE2.Image = CType(resources.GetObject("ALFILNEGRE2.Image"), System.Drawing.Image)
        Me.ALFILNEGRE2.Location = New System.Drawing.Point(119, 12)
        Me.ALFILNEGRE2.Name = "ALFILNEGRE2"
        Me.ALFILNEGRE2.Size = New System.Drawing.Size(44, 41)
        Me.ALFILNEGRE2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ALFILNEGRE2.TabIndex = 102
        Me.ALFILNEGRE2.TabStop = False
        Me.ALFILNEGRE2.Tag = "28"
        '
        'TORREBLANCA2
        '
        Me.TORREBLANCA2.Image = CType(resources.GetObject("TORREBLANCA2.Image"), System.Drawing.Image)
        Me.TORREBLANCA2.Location = New System.Drawing.Point(14, 349)
        Me.TORREBLANCA2.Name = "TORREBLANCA2"
        Me.TORREBLANCA2.Size = New System.Drawing.Size(44, 41)
        Me.TORREBLANCA2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.TORREBLANCA2.TabIndex = 103
        Me.TORREBLANCA2.TabStop = False
        Me.TORREBLANCA2.Tag = "18"
        '
        'PEOBLANC8
        '
        Me.PEOBLANC8.Image = CType(resources.GetObject("PEOBLANC8.Image"), System.Drawing.Image)
        Me.PEOBLANC8.Location = New System.Drawing.Point(16, 304)
        Me.PEOBLANC8.Name = "PEOBLANC8"
        Me.PEOBLANC8.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC8.TabIndex = 104
        Me.PEOBLANC8.TabStop = False
        Me.PEOBLANC8.Tag = "8"
        '
        'PEOBLANC7
        '
        Me.PEOBLANC7.Image = CType(resources.GetObject("PEOBLANC7.Image"), System.Drawing.Image)
        Me.PEOBLANC7.Location = New System.Drawing.Point(62, 301)
        Me.PEOBLANC7.Name = "PEOBLANC7"
        Me.PEOBLANC7.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC7.TabIndex = 105
        Me.PEOBLANC7.TabStop = False
        Me.PEOBLANC7.Tag = "7"
        '
        'PEOBLANC6
        '
        Me.PEOBLANC6.Image = CType(resources.GetObject("PEOBLANC6.Image"), System.Drawing.Image)
        Me.PEOBLANC6.Location = New System.Drawing.Point(119, 301)
        Me.PEOBLANC6.Name = "PEOBLANC6"
        Me.PEOBLANC6.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC6.TabIndex = 106
        Me.PEOBLANC6.TabStop = False
        Me.PEOBLANC6.Tag = "6"
        '
        'PEOBLANC5
        '
        Me.PEOBLANC5.Image = CType(resources.GetObject("PEOBLANC5.Image"), System.Drawing.Image)
        Me.PEOBLANC5.Location = New System.Drawing.Point(173, 301)
        Me.PEOBLANC5.Name = "PEOBLANC5"
        Me.PEOBLANC5.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC5.TabIndex = 107
        Me.PEOBLANC5.TabStop = False
        Me.PEOBLANC5.Tag = "5"
        '
        'PEOBLANC4
        '
        Me.PEOBLANC4.Image = CType(resources.GetObject("PEOBLANC4.Image"), System.Drawing.Image)
        Me.PEOBLANC4.Location = New System.Drawing.Point(223, 301)
        Me.PEOBLANC4.Name = "PEOBLANC4"
        Me.PEOBLANC4.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC4.TabIndex = 108
        Me.PEOBLANC4.TabStop = False
        Me.PEOBLANC4.Tag = "4"
        '
        'PEOBLANC3
        '
        Me.PEOBLANC3.Image = CType(resources.GetObject("PEOBLANC3.Image"), System.Drawing.Image)
        Me.PEOBLANC3.Location = New System.Drawing.Point(273, 301)
        Me.PEOBLANC3.Name = "PEOBLANC3"
        Me.PEOBLANC3.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC3.TabIndex = 109
        Me.PEOBLANC3.TabStop = False
        Me.PEOBLANC3.Tag = "3"
        '
        'PEOBLANC2
        '
        Me.PEOBLANC2.Image = CType(resources.GetObject("PEOBLANC2.Image"), System.Drawing.Image)
        Me.PEOBLANC2.Location = New System.Drawing.Point(325, 301)
        Me.PEOBLANC2.Name = "PEOBLANC2"
        Me.PEOBLANC2.Size = New System.Drawing.Size(44, 41)
        Me.PEOBLANC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PEOBLANC2.TabIndex = 110
        Me.PEOBLANC2.TabStop = False
        Me.PEOBLANC2.Tag = "2"
        '
        'ALFILBLANC2
        '
        Me.ALFILBLANC2.Image = CType(resources.GetObject("ALFILBLANC2.Image"), System.Drawing.Image)
        Me.ALFILBLANC2.Location = New System.Drawing.Point(119, 348)
        Me.ALFILBLANC2.Name = "ALFILBLANC2"
        Me.ALFILBLANC2.Size = New System.Drawing.Size(44, 41)
        Me.ALFILBLANC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ALFILBLANC2.TabIndex = 111
        Me.ALFILBLANC2.TabStop = False
        Me.ALFILBLANC2.Tag = "26"
        '
        'CAVALLBLANC2
        '
        Me.CAVALLBLANC2.Image = CType(resources.GetObject("CAVALLBLANC2.Image"), System.Drawing.Image)
        Me.CAVALLBLANC2.Location = New System.Drawing.Point(62, 349)
        Me.CAVALLBLANC2.Name = "CAVALLBLANC2"
        Me.CAVALLBLANC2.Size = New System.Drawing.Size(44, 41)
        Me.CAVALLBLANC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CAVALLBLANC2.TabIndex = 112
        Me.CAVALLBLANC2.TabStop = False
        Me.CAVALLBLANC2.Tag = "22"
        '
        'TAULELL
        '
        Me.TAULELL.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TAULELL.Image = CType(resources.GetObject("TAULELL.Image"), System.Drawing.Image)
        Me.TAULELL.Location = New System.Drawing.Point(1, 1)
        Me.TAULELL.Name = "TAULELL"
        Me.TAULELL.Size = New System.Drawing.Size(431, 401)
        Me.TAULELL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.TAULELL.TabIndex = 113
        Me.TAULELL.TabStop = False
        '
        'FINALITZAR
        '
        Me.FINALITZAR.Location = New System.Drawing.Point(701, 12)
        Me.FINALITZAR.Name = "FINALITZAR"
        Me.FINALITZAR.Size = New System.Drawing.Size(87, 23)
        Me.FINALITZAR.TabIndex = 114
        Me.FINALITZAR.Text = "FINALITZAR"
        Me.FINALITZAR.UseVisualStyleBackColor = True
        '
        'PARTIDAACTIVA
        '
        Me.PARTIDAACTIVA.Location = New System.Drawing.Point(555, 366)
        Me.PARTIDAACTIVA.Name = "PARTIDAACTIVA"
        Me.PARTIDAACTIVA.Size = New System.Drawing.Size(75, 23)
        Me.PARTIDAACTIVA.TabIndex = 115
        Me.PARTIDAACTIVA.Text = "ACTIVA"
        Me.PARTIDAACTIVA.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.TextBox1.Location = New System.Drawing.Point(571, 335)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(130, 20)
        Me.TextBox1.TabIndex = 116
        Me.TextBox1.Text = "ESTAT DE LA PARTIDA"
        '
        'PARTIDAFINALITZADA
        '
        Me.PARTIDAFINALITZADA.Location = New System.Drawing.Point(636, 366)
        Me.PARTIDAFINALITZADA.Name = "PARTIDAFINALITZADA"
        Me.PARTIDAFINALITZADA.Size = New System.Drawing.Size(91, 23)
        Me.PARTIDAFINALITZADA.TabIndex = 117
        Me.PARTIDAFINALITZADA.Text = "FINALITZADA"
        Me.PARTIDAFINALITZADA.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 405)
        Me.Controls.Add(Me.PARTIDAFINALITZADA)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.PARTIDAACTIVA)
        Me.Controls.Add(Me.FINALITZAR)
        Me.Controls.Add(Me.CAVALLBLANC2)
        Me.Controls.Add(Me.ALFILBLANC2)
        Me.Controls.Add(Me.PEOBLANC2)
        Me.Controls.Add(Me.PEOBLANC3)
        Me.Controls.Add(Me.PEOBLANC4)
        Me.Controls.Add(Me.PEOBLANC5)
        Me.Controls.Add(Me.PEOBLANC6)
        Me.Controls.Add(Me.PEOBLANC7)
        Me.Controls.Add(Me.PEOBLANC8)
        Me.Controls.Add(Me.TORREBLANCA2)
        Me.Controls.Add(Me.ALFILNEGRE2)
        Me.Controls.Add(Me.PEONEGRE2)
        Me.Controls.Add(Me.CAVALLNEGRE2)
        Me.Controls.Add(Me.PEONEGRE3)
        Me.Controls.Add(Me.PEONEGRE4)
        Me.Controls.Add(Me.PEONEGRE5)
        Me.Controls.Add(Me.PEONEGRE6)
        Me.Controls.Add(Me.PEONEGRE7)
        Me.Controls.Add(Me.PEONEGRE8)
        Me.Controls.Add(Me.REINEGRE)
        Me.Controls.Add(Me.CAVALLNEGRE1)
        Me.Controls.Add(Me.PEONEGRE1)
        Me.Controls.Add(Me.TORREBLANCA1)
        Me.Controls.Add(Me.REINANEGRA)
        Me.Controls.Add(Me.ALFILNEGRE1)
        Me.Controls.Add(Me.REINABLANCA)
        Me.Controls.Add(Me.ALFILBLANC1)
        Me.Controls.Add(Me.CAVALLBLANC1)
        Me.Controls.Add(Me.REIBLANC)
        Me.Controls.Add(Me.PEOBLANC1)
        Me.Controls.Add(Me.TORRENEGRA2)
        Me.Controls.Add(Me.TORRENEGRA1)
        Me.Controls.Add(Me.H5)
        Me.Controls.Add(Me.H8)
        Me.Controls.Add(Me.H7)
        Me.Controls.Add(Me.H6)
        Me.Controls.Add(Me.H4)
        Me.Controls.Add(Me.H2)
        Me.Controls.Add(Me.H3)
        Me.Controls.Add(Me.H1)
        Me.Controls.Add(Me.G8)
        Me.Controls.Add(Me.G5)
        Me.Controls.Add(Me.G6)
        Me.Controls.Add(Me.G7)
        Me.Controls.Add(Me.G4)
        Me.Controls.Add(Me.G3)
        Me.Controls.Add(Me.F5)
        Me.Controls.Add(Me.F6)
        Me.Controls.Add(Me.F7)
        Me.Controls.Add(Me.F8)
        Me.Controls.Add(Me.G1)
        Me.Controls.Add(Me.G2)
        Me.Controls.Add(Me.E6)
        Me.Controls.Add(Me.E7)
        Me.Controls.Add(Me.E8)
        Me.Controls.Add(Me.F1)
        Me.Controls.Add(Me.F2)
        Me.Controls.Add(Me.F3)
        Me.Controls.Add(Me.F4)
        Me.Controls.Add(Me.A2)
        Me.Controls.Add(Me.A3)
        Me.Controls.Add(Me.A4)
        Me.Controls.Add(Me.A5)
        Me.Controls.Add(Me.A6)
        Me.Controls.Add(Me.A7)
        Me.Controls.Add(Me.A8)
        Me.Controls.Add(Me.B1)
        Me.Controls.Add(Me.B2)
        Me.Controls.Add(Me.B3)
        Me.Controls.Add(Me.B4)
        Me.Controls.Add(Me.B5)
        Me.Controls.Add(Me.B6)
        Me.Controls.Add(Me.B7)
        Me.Controls.Add(Me.B8)
        Me.Controls.Add(Me.C1)
        Me.Controls.Add(Me.C2)
        Me.Controls.Add(Me.C3)
        Me.Controls.Add(Me.C4)
        Me.Controls.Add(Me.C5)
        Me.Controls.Add(Me.C6)
        Me.Controls.Add(Me.C7)
        Me.Controls.Add(Me.C8)
        Me.Controls.Add(Me.D1)
        Me.Controls.Add(Me.D2)
        Me.Controls.Add(Me.D3)
        Me.Controls.Add(Me.D4)
        Me.Controls.Add(Me.D5)
        Me.Controls.Add(Me.D6)
        Me.Controls.Add(Me.D7)
        Me.Controls.Add(Me.D8)
        Me.Controls.Add(Me.E1)
        Me.Controls.Add(Me.E2)
        Me.Controls.Add(Me.E3)
        Me.Controls.Add(Me.E4)
        Me.Controls.Add(Me.E5)
        Me.Controls.Add(Me.A1)
        Me.Controls.Add(Me.TORNBLANQUES)
        Me.Controls.Add(Me.TORNNEGRES)
        Me.Controls.Add(Me.TORN)
        Me.Controls.Add(Me.TAULELL)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.TORRENEGRA1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TORRENEGRA2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.REIBLANC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CAVALLBLANC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALFILBLANC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.REINABLANCA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALFILNEGRE1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.REINANEGRA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TORREBLANCA1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CAVALLNEGRE1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.REINEGRE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CAVALLNEGRE2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEONEGRE2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALFILNEGRE2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TORREBLANCA2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PEOBLANC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALFILBLANC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CAVALLBLANC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TAULELL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TORN As Button
    Friend WithEvents TORNNEGRES As Button
    Friend WithEvents TORNBLANQUES As Button
    Friend WithEvents TORRENEGRA1 As PictureBox
    Friend WithEvents A1 As PictureBox
    Friend WithEvents E5 As PictureBox
    Friend WithEvents E4 As PictureBox
    Friend WithEvents E3 As PictureBox
    Friend WithEvents E2 As PictureBox
    Friend WithEvents E1 As PictureBox
    Friend WithEvents D8 As PictureBox
    Friend WithEvents D7 As PictureBox
    Friend WithEvents D6 As PictureBox
    Friend WithEvents D5 As PictureBox
    Friend WithEvents D4 As PictureBox
    Friend WithEvents D3 As PictureBox
    Friend WithEvents D2 As PictureBox
    Friend WithEvents D1 As PictureBox
    Friend WithEvents C8 As PictureBox
    Friend WithEvents C7 As PictureBox
    Friend WithEvents C6 As PictureBox
    Friend WithEvents C5 As PictureBox
    Friend WithEvents C4 As PictureBox
    Friend WithEvents C3 As PictureBox
    Friend WithEvents C2 As PictureBox
    Friend WithEvents C1 As PictureBox
    Friend WithEvents B8 As PictureBox
    Friend WithEvents B7 As PictureBox
    Friend WithEvents B6 As PictureBox
    Friend WithEvents B5 As PictureBox
    Friend WithEvents B4 As PictureBox
    Friend WithEvents B3 As PictureBox
    Friend WithEvents B2 As PictureBox
    Friend WithEvents B1 As PictureBox
    Friend WithEvents A8 As PictureBox
    Friend WithEvents A7 As PictureBox
    Friend WithEvents A6 As PictureBox
    Friend WithEvents A5 As PictureBox
    Friend WithEvents A4 As PictureBox
    Friend WithEvents A3 As PictureBox
    Friend WithEvents A2 As PictureBox
    Friend WithEvents F4 As PictureBox
    Friend WithEvents F3 As PictureBox
    Friend WithEvents F2 As PictureBox
    Friend WithEvents F1 As PictureBox
    Friend WithEvents E8 As PictureBox
    Friend WithEvents E7 As PictureBox
    Friend WithEvents E6 As PictureBox
    Friend WithEvents G2 As PictureBox
    Friend WithEvents G1 As PictureBox
    Friend WithEvents F8 As PictureBox
    Friend WithEvents F7 As PictureBox
    Friend WithEvents F6 As PictureBox
    Friend WithEvents F5 As PictureBox
    Friend WithEvents G3 As PictureBox
    Friend WithEvents G4 As PictureBox
    Friend WithEvents G7 As PictureBox
    Friend WithEvents G6 As PictureBox
    Friend WithEvents G5 As PictureBox
    Friend WithEvents G8 As PictureBox
    Friend WithEvents H1 As PictureBox
    Friend WithEvents H3 As PictureBox
    Friend WithEvents H2 As PictureBox
    Friend WithEvents H4 As PictureBox
    Friend WithEvents H6 As PictureBox
    Friend WithEvents H7 As PictureBox
    Friend WithEvents H8 As PictureBox
    Friend WithEvents H5 As PictureBox
    Friend WithEvents TORRENEGRA2 As PictureBox
    Friend WithEvents PEOBLANC1 As PictureBox
    Friend WithEvents REIBLANC As PictureBox
    Friend WithEvents CAVALLBLANC1 As PictureBox
    Friend WithEvents ALFILBLANC1 As PictureBox
    Friend WithEvents REINABLANCA As PictureBox
    Friend WithEvents ALFILNEGRE1 As PictureBox
    Friend WithEvents REINANEGRA As PictureBox
    Friend WithEvents TORREBLANCA1 As PictureBox
    Friend WithEvents PEONEGRE1 As PictureBox
    Friend WithEvents CAVALLNEGRE1 As PictureBox
    Friend WithEvents REINEGRE As PictureBox
    Friend WithEvents PEONEGRE8 As PictureBox
    Friend WithEvents PEONEGRE7 As PictureBox
    Friend WithEvents PEONEGRE6 As PictureBox
    Friend WithEvents PEONEGRE5 As PictureBox
    Friend WithEvents PEONEGRE4 As PictureBox
    Friend WithEvents PEONEGRE3 As PictureBox
    Friend WithEvents CAVALLNEGRE2 As PictureBox
    Friend WithEvents PEONEGRE2 As PictureBox
    Friend WithEvents ALFILNEGRE2 As PictureBox
    Friend WithEvents TORREBLANCA2 As PictureBox
    Friend WithEvents PEOBLANC8 As PictureBox
    Friend WithEvents PEOBLANC7 As PictureBox
    Friend WithEvents PEOBLANC6 As PictureBox
    Friend WithEvents PEOBLANC5 As PictureBox
    Friend WithEvents PEOBLANC4 As PictureBox
    Friend WithEvents PEOBLANC3 As PictureBox
    Friend WithEvents PEOBLANC2 As PictureBox
    Friend WithEvents ALFILBLANC2 As PictureBox
    Friend WithEvents CAVALLBLANC2 As PictureBox
    Friend WithEvents TAULELL As PictureBox
    Friend WithEvents FINALITZAR As Button
    Friend WithEvents PARTIDAACTIVA As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents PARTIDAFINALITZADA As Button
End Class
