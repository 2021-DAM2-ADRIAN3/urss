﻿Public Class Form1
    'creacio dels arrays de caselles i peces ja que per poder moure les peces necesitem que es situin a sobre de les caselles'
    Dim caselles() As PictureBox
    Dim peces() As PictureBox
    'creacio de la variable premuda, que la utilitzarem per poder fer el moviment de les peces, canviant el seu valor'
    Dim premuda As Int32
    'creacio del vector i per poder fer bucles i reduir linies de codi'
    Dim i As Int32
    'creacio d'un contador de forma booleana per configurar el torn dels participants'
    Dim contador As Boolean = True
    Dim contadorfinalitzar As Boolean = True

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'color de fons'
        BackColor = Color.Azure
        'emplenem els array amb el nom de cada peça o casella que hem creat'
        peces = New PictureBox() {PEOBLANC1, PEOBLANC2, PEOBLANC3, PEOBLANC4, PEOBLANC5, PEOBLANC6, PEOBLANC7, PEOBLANC8, PEONEGRE1, PEONEGRE2,
         PEONEGRE3, PEONEGRE4, PEONEGRE5, PEONEGRE6, PEONEGRE7, PEONEGRE8, TORREBLANCA1, TORREBLANCA2, TORRENEGRA1, TORRENEGRA2, CAVALLBLANC1, CAVALLBLANC2, CAVALLNEGRE1,
         CAVALLNEGRE2, ALFILBLANC1, ALFILBLANC2, ALFILNEGRE1, ALFILNEGRE2, REIBLANC, REINABLANCA, REINANEGRA, REINEGRE}
        caselles = New PictureBox() {A1, A2, A3, A4, A5, A6, A7, A8, B1, B2, B3, B4, B5, B6, B7, B8, C1, C2, C3, C4, C5, C6, C7, C8, D1, D2, D3, D4, D5, D6, D7, D8,
        E1, E2, E3, E4, E5, E6, E7, E8, F1, F2, F3, F4, F5, F6, F7, F8, G1, G2, G3, G4, G5, G6, G7, G8, H1, H2, H3, H4, H5, H6, H7, H8}

        'bucle per fer la transparencia de les caselles per un millor aspecte visual'
        For i = 0 To 63
            caselles(i).Parent = TAULELL
            caselles(i).BackColor = Color.Transparent
        Next


        'bucle per fer la transparencia de les peces per un millor aspecte visual'
        For i = 0 To 31
            peces(i).BringToFront()
            peces(i).BackColor = Color.Transparent
            peces(i).Top = 0
            peces(i).Left = 0
        Next

        'independentment del bucle de les peces i la seva transparencia, necesitem saber la posicio en la que estàn inicialment'
        TORRENEGRA1.Parent = H8
        TORRENEGRA2.Parent = A8
        CAVALLNEGRE1.Parent = G8
        CAVALLNEGRE2.Parent = B8
        ALFILNEGRE1.Parent = F8
        ALFILNEGRE2.Parent = C8
        REINEGRE.Parent = E8
        REINANEGRA.Parent = D8
        PEONEGRE1.Parent = H7
        PEONEGRE2.Parent = G7
        PEONEGRE3.Parent = F7
        PEONEGRE4.Parent = E7
        PEONEGRE5.Parent = D7
        PEONEGRE6.Parent = C7
        PEONEGRE7.Parent = B7
        PEONEGRE8.Parent = A7


        TORREBLANCA1.Parent = H1
        TORREBLANCA2.Parent = A1
        CAVALLBLANC1.Parent = G1
        CAVALLBLANC2.Parent = B1
        ALFILBLANC1.Parent = F1
        ALFILBLANC2.Parent = C1
        REIBLANC.Parent = E1
        REINABLANCA.Parent = D1
        PEOBLANC8.Parent = A2
        PEOBLANC7.Parent = B2
        PEOBLANC6.Parent = C2
        PEOBLANC5.Parent = D2
        PEOBLANC4.Parent = E2
        PEOBLANC3.Parent = F2
        PEOBLANC2.Parent = G2
        PEOBLANC1.Parent = H2

        PARTIDAACTIVA.BackColor = Color.Green
    End Sub



    'codi el qual utilitzem per  poder fer el moviment de les peces, canviant el valor de premuda, per la posicio que ocupa al array de peces declarant anteriorment'
    Private Sub TORRENEGRA1_Click(sender As Object, e As MouseEventArgs) Handles TORRENEGRA1.Click
        premuda = 18
    End Sub

    Private Sub TORRENEGRA2_Click(sender As Object, e As MouseEventArgs) Handles TORRENEGRA2.Click
        premuda = 19

    End Sub
    Private Sub CAVALLNEGRE1_Click(sender As Object, e As MouseEventArgs) Handles CAVALLNEGRE1.Click
        premuda = 22

    End Sub
    Private Sub CAVALLNEGRE2_Click(sender As Object, e As MouseEventArgs) Handles CAVALLNEGRE2.Click
        premuda = 23

    End Sub
    Private Sub ALFILNEGRE1_Click(sender As Object, e As MouseEventArgs) Handles ALFILNEGRE1.Click
        premuda = 26

    End Sub
    Private Sub ALFILNEGRE2_Click(sender As Object, e As MouseEventArgs) Handles ALFILNEGRE2.Click
        premuda = 27

    End Sub
    Private Sub REINEGRE_Click(sender As Object, e As MouseEventArgs) Handles REINEGRE.Click
        premuda = 31

    End Sub
    Private Sub REINANEGRA_Click(sender As Object, e As MouseEventArgs) Handles REINANEGRA.Click
        premuda = 30

    End Sub
    Private Sub PEONEGRE1_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE1.Click
        premuda = 8

    End Sub
    Private Sub PEONEGRE2_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE2.Click
        premuda = 9

    End Sub
    Private Sub PEONEGRE3_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE3.Click
        premuda = 10

    End Sub
    Private Sub PEONEGRE4_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE4.Click
        premuda = 11

    End Sub
    Private Sub PEONEGRE5_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE5.Click
        premuda = 12

    End Sub
    Private Sub PEONECRE6_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE6.Click
        premuda = 13

    End Sub
    Private Sub PEONEGRE7_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE7.Click
        premuda = 14

    End Sub
    Private Sub PEONEGRE8_Click(sender As Object, e As MouseEventArgs) Handles PEONEGRE8.Click
        premuda = 15

    End Sub
    Private Sub TORREBLANCA1_Click(sender As Object, e As MouseEventArgs) Handles TORREBLANCA1.Click
        premuda = 16

    End Sub
    Private Sub TORREBLANCA2_Click(sender As Object, e As MouseEventArgs) Handles TORREBLANCA2.Click
        premuda = 17

    End Sub
    Private Sub CAVALLBLANC1_Click(sender As Object, e As MouseEventArgs) Handles CAVALLBLANC1.Click
        premuda = 20

    End Sub
    Private Sub CAVALLBLANC2_Click(sender As Object, e As MouseEventArgs) Handles CAVALLBLANC2.Click
        premuda = 21

    End Sub
    Private Sub ALFILBLANC1_Click(sender As Object, e As MouseEventArgs) Handles ALFILBLANC1.Click
        premuda = 24

    End Sub
    Private Sub ALFILBLANC2_Click(sender As Object, e As MouseEventArgs) Handles ALFILBLANC2.Click
        premuda = 25

    End Sub
    Private Sub REINABLANCA_Click(sender As Object, e As MouseEventArgs) Handles REINABLANCA.Click
        premuda = 29

    End Sub
    Private Sub REIBLANC_Click(sender As Object, e As MouseEventArgs) Handles REIBLANC.Click
        premuda = 28

    End Sub
    Private Sub PEOBLANC1_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC1.Click
        premuda = 0

    End Sub
    Private Sub PEOBLANC2_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC2.Click
        premuda = 1

    End Sub
    Private Sub PEOBLANC3_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC3.Click
        premuda = 2

    End Sub
    Private Sub PEOBLANC4_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC4.Click
        premuda = 3

    End Sub
    Private Sub PEOBLANC5_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC5.Click
        premuda = 4

    End Sub
    Private Sub PEOBLANC6_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC6.Click
        premuda = 5

    End Sub
    Private Sub PEOBLANC7_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC7.Click
        premuda = 6

    End Sub
    Private Sub PEOBLANC8_Click(sender As Object, e As MouseEventArgs) Handles PEOBLANC8.Click
        premuda = 7

    End Sub



    'troç de codi que utilitzem per moure les pecess canviant el valor de peces(premuda) per la posicio de la casella que haguem elegit'
    Private Sub A1_Click(sender As Object, e As EventArgs) Handles A1.Click
        peces(premuda).Parent = A1
    End Sub

    Private Sub A2_Click(sender As Object, e As EventArgs) Handles A2.Click
        peces(premuda).Parent = A2
    End Sub

    Private Sub A3_Click(sender As Object, e As EventArgs) Handles A3.Click
        peces(premuda).Parent = A3
    End Sub

    Private Sub A4_Click(sender As Object, e As EventArgs) Handles A4.Click
        peces(premuda).Parent = A4
    End Sub

    Private Sub A5_Click(sender As Object, e As EventArgs) Handles A5.Click
        peces(premuda).Parent = A5
    End Sub
    Private Sub A6_Click(sender As Object, e As EventArgs) Handles A6.Click
        peces(premuda).Parent = A6
    End Sub
    Private Sub A7_Click(sender As Object, e As EventArgs) Handles A7.Click
        peces(premuda).Parent = A7
    End Sub
    Private Sub A8_Click(sender As Object, e As EventArgs) Handles A8.Click
        peces(premuda).Parent = A8
    End Sub
    Private Sub B1_Click(sender As Object, e As EventArgs) Handles B1.Click
        peces(premuda).Parent = B1
    End Sub
    Private Sub B2_Click(sender As Object, e As EventArgs) Handles B2.Click
        peces(premuda).Parent = B2
    End Sub
    Private Sub B3_Click(sender As Object, e As EventArgs) Handles B3.Click
        peces(premuda).Parent = B3
    End Sub
    Private Sub B4_Click(sender As Object, e As EventArgs) Handles B4.Click
        peces(premuda).Parent = B4
    End Sub
    Private Sub B5_Click(sender As Object, e As EventArgs) Handles B5.Click
        peces(premuda).Parent = B5
    End Sub
    Private Sub B6_Click(sender As Object, e As EventArgs) Handles B6.Click
        peces(premuda).Parent = B6
    End Sub
    Private Sub B7_Click(sender As Object, e As EventArgs) Handles B7.Click
        peces(premuda).Parent = B7
    End Sub
    Private Sub B8_Click(sender As Object, e As EventArgs) Handles B8.Click
        peces(premuda).Parent = B8
    End Sub
    Private Sub C1_Click(sender As Object, e As EventArgs) Handles C1.Click
        peces(premuda).Parent = C1
    End Sub
    Private Sub C2_Click(sender As Object, e As EventArgs) Handles C2.Click
        peces(premuda).Parent = C2
    End Sub
    Private Sub C3_Click(sender As Object, e As EventArgs) Handles C3.Click
        peces(premuda).Parent = C3
    End Sub
    Private Sub C4_Click(sender As Object, e As EventArgs) Handles C4.Click
        peces(premuda).Parent = C4
    End Sub
    Private Sub C5_Click(sender As Object, e As EventArgs) Handles C5.Click
        peces(premuda).Parent = C5
    End Sub
    Private Sub C6_Click(sender As Object, e As EventArgs) Handles C6.Click
        peces(premuda).Parent = C6
    End Sub
    Private Sub C7_Click(sender As Object, e As EventArgs) Handles C7.Click
        peces(premuda).Parent = C7
    End Sub
    Private Sub C8_Click(sender As Object, e As EventArgs) Handles C8.Click
        peces(premuda).Parent = C8
    End Sub
    Private Sub D1_Click(sender As Object, e As EventArgs) Handles D1.Click
        peces(premuda).Parent = D1
    End Sub
    Private Sub D2_Click(sender As Object, e As EventArgs) Handles D2.Click
        peces(premuda).Parent = D2
    End Sub
    Private Sub D3_Click(sender As Object, e As EventArgs) Handles D3.Click
        peces(premuda).Parent = D3
    End Sub
    Private Sub D4_Click(sender As Object, e As EventArgs) Handles D4.Click
        peces(premuda).Parent = D4
    End Sub
    Private Sub D5_Click(sender As Object, e As EventArgs) Handles D5.Click
        peces(premuda).Parent = D5
    End Sub
    Private Sub D6_Click(sender As Object, e As EventArgs) Handles D6.Click
        peces(premuda).Parent = D6
    End Sub
    Private Sub D7_Click(sender As Object, e As EventArgs) Handles D7.Click
        peces(premuda).Parent = D7
    End Sub
    Private Sub D8_Click(sender As Object, e As EventArgs) Handles D8.Click
        peces(premuda).Parent = D8
    End Sub
    Private Sub E1_Click(sender As Object, e As EventArgs) Handles E1.Click
        peces(premuda).Parent = E1
    End Sub
    Private Sub E2_Click(sender As Object, e As EventArgs) Handles E2.Click
        peces(premuda).Parent = E2
    End Sub
    Private Sub E3_Click(sender As Object, e As EventArgs) Handles E3.Click
        peces(premuda).Parent = E3
    End Sub
    Private Sub E4_Click(sender As Object, e As EventArgs) Handles E4.Click
        peces(premuda).Parent = E4
    End Sub
    Private Sub E5_Click(sender As Object, e As EventArgs) Handles E5.Click
        peces(premuda).Parent = E5
    End Sub
    Private Sub E6_Click(sender As Object, e As EventArgs) Handles E6.Click
        peces(premuda).Parent = E6
    End Sub
    Private Sub E7_Click(sender As Object, e As EventArgs) Handles E7.Click
        peces(premuda).Parent = E7
    End Sub
    Private Sub E8_Click(sender As Object, e As EventArgs) Handles E8.Click
        peces(premuda).Parent = E8
    End Sub
    Private Sub F1_Click(sender As Object, e As EventArgs) Handles F1.Click
        peces(premuda).Parent = F1
    End Sub
    Private Sub F2_Click(sender As Object, e As EventArgs) Handles F2.Click
        peces(premuda).Parent = F2
    End Sub
    Private Sub F3_Click(sender As Object, e As EventArgs) Handles F3.Click
        peces(premuda).Parent = F3
    End Sub
    Private Sub F4_Click(sender As Object, e As EventArgs) Handles F4.Click
        peces(premuda).Parent = F4
    End Sub
    Private Sub F5_Click(sender As Object, e As EventArgs) Handles F5.Click
        peces(premuda).Parent = F5
    End Sub
    Private Sub F6_Click(sender As Object, e As EventArgs) Handles F6.Click
        peces(premuda).Parent = F6
    End Sub
    Private Sub F7_Click(sender As Object, e As EventArgs) Handles F7.Click
        peces(premuda).Parent = F7
    End Sub
    Private Sub F8_Click(sender As Object, e As EventArgs) Handles F8.Click
        peces(premuda).Parent = F8
    End Sub
    Private Sub G1_Click(sender As Object, e As EventArgs) Handles G1.Click
        peces(premuda).Parent = G1
    End Sub
    Private Sub G2_Click(sender As Object, e As EventArgs) Handles G2.Click
        peces(premuda).Parent = G2
    End Sub
    Private Sub G3_Click(sender As Object, e As EventArgs) Handles G3.Click
        peces(premuda).Parent = G3
    End Sub
    Private Sub G4_Click(sender As Object, e As EventArgs) Handles G4.Click
        peces(premuda).Parent = G4
    End Sub
    Private Sub G5_Click(sender As Object, e As EventArgs) Handles G5.Click
        peces(premuda).Parent = G5
    End Sub
    Private Sub G6_Click(sender As Object, e As EventArgs) Handles G6.Click
        peces(premuda).Parent = G6
    End Sub
    Private Sub G7_Click(sender As Object, e As EventArgs) Handles G7.Click
        peces(premuda).Parent = G7
    End Sub
    Private Sub G8_Click(sender As Object, e As EventArgs) Handles G8.Click
        peces(premuda).Parent = G8
    End Sub
    Private Sub H1_Click(sender As Object, e As EventArgs) Handles H1.Click
        peces(premuda).Parent = H1
    End Sub
    Private Sub H2_Click(sender As Object, e As EventArgs) Handles H2.Click
        peces(premuda).Parent = H2
    End Sub
    Private Sub H3_Click(sender As Object, e As EventArgs) Handles H3.Click
        peces(premuda).Parent = H3
    End Sub
    Private Sub H4_Click(sender As Object, e As EventArgs) Handles H4.Click
        peces(premuda).Parent = H4
    End Sub
    Private Sub H5_Click(sender As Object, e As EventArgs) Handles H5.Click
        peces(premuda).Parent = H5
    End Sub
    Private Sub H6_Click(sender As Object, e As EventArgs) Handles H6.Click
        peces(premuda).Parent = H6
    End Sub
    Private Sub H7_Click(sender As Object, e As EventArgs) Handles H7.Click
        peces(premuda).Parent = H7
    End Sub
    Private Sub H8_Click(sender As Object, e As EventArgs) Handles H8.Click
        peces(premuda).Parent = H8
    End Sub


    Private Sub TORN_Click(sender As Object, e As EventArgs) Handles TORN.Click
        'creacio de un condicional per controlar el torn del jugadors, utilitzant la variable booleana creada anteriorment'
        If contador = True Then
            TORNBLANQUES.BackColor = Color.GreenYellow
            TORNNEGRES.BackColor = Color.Transparent
            contador = False
        Else
            TORNNEGRES.BackColor = Color.GreenYellow
            TORNBLANQUES.BackColor = Color.Transparent
            contador = True
        End If



    End Sub
    'creo el boto finalitzar per acabar la partida, aquest al pitjar-lo, farà canviar de color els botons de partida finalitzada i partida activa'
    Private Sub FINALITZAR_Click(sender As Object, e As EventArgs) Handles FINALITZAR.Click
        PARTIDAFINALITZADA.BackColor = Color.Red
        PARTIDAACTIVA.BackColor = Color.Transparent
    End Sub
    'he intentat crear la funcio per matar, ficant al tag de cada peça, el numero que ocupa cada una al array' 
    Private Sub matar(ByVal casella As PictureBox)
        If contador = True Then
            peces(premuda).Parent = Tag.parent
        Else
            peces(premuda) = casella
        End If
    End Sub

End Class


